<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <title>Exemplo de Javascript</title>
    </head>
    <body>
        <a href="javascript:;" id="btnAlerta">Mostrar Alerta em Javascript</a>   
        <br>
        <a href="https://google.com" target ="_blank">Google</a>

        <script>
        let alerta = document.getElementById("btnAlerta");
        alerta.onclick = function() {
            alert("Este é um exemplo de javascript");
        }

        </script>
    </body>
</html>