<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aula de HTML</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

</head>
  <body>
    <h1>Olá, mundo!</h1>
    <button type="button" class="btn btn-primary">Cadastrar</button>

    <a href="https://google.com" target ="_blank" class="btn btn-danger">Google</a>
    <hr>
    <h2>Lista de Clientes</h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Ação</th>
             </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Daniel Nichiata</td>
                <td>danielnichiata@gmail.com</td>
                <td>
                    <a href="#" class="btn btn-primary">Editar</a>
                    <a href="#" class="btn btn-danger">Excluir</a>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Samirah Kurdi</td>
                <td>samirah_sk@gmail.com</td>
                <td>
                    <a href="#" class="btn btn-primary">Editar</a>
                    <a href="#" class="btn btn-danger">Excluir</a>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Ana Maria</td>
                <td>anamaria@gmail.com</td>
                <td>
                    <a href="#" class="btn btn-primary">Editar</a>
                    <a href="#" class="btn btn-danger">Excluir</a>
                </td>
            </tr>

        </tbody>
    </table>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>