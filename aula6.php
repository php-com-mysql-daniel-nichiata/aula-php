<?php

echo "<h1>Laço de repetiçao</h1>";
echo "<h2>While</h2>";

$contador = 1;

while($contador = 1) {
    echo $contador . " ";
    $contador++;
}

echo "<hr>";
echo "<h2>For</h2>";

for ($contador=1; $contador <= 10 ; $contador++) { 
    echo $contador . " ";
}

echo "<hr>";
echo "<h2>do while</h2>";

$contador = 1;

do{
    echo $contador . " ";
    $contador++;
}while($contador <= 10);