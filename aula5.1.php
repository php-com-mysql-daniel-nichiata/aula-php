<?php

echo "<h1>Estrutura condicional</h1>";

$nome = "Daniel Nichiata";
$idade = 18;
$email = "danielnichiata@gmail.com";
$senha = "12345678";

////////////////////////////

echo "<h4>...If()...</h4>";

if($idade >= 18){
    echo "O usuário $nome é maior de idade";
}


echo"<hr>";
///////////////////////////
echo "<h4>...If() e else...</h4>";

$idade = 17;

if($idade >= 18){
    echo "O usuário $nome é maior de idade";
} else {
    echo "O usuário $nome é menor de idade";
}