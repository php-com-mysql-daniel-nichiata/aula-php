<?php

$listaCompra = ['arroz', "banana", "feijão"];

echo $listaCompra[1];

echo "<hr>";

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo "<hr>";

$usuario = [
    "nome" => "Edson Rodrigues",
    "e-mail" => "edson@test.com"
];



echo $usuarios['nome'];


echo "<hr>";

$usuarios = [
    [
        "nome" => "Edson Rodrigues",
        "email" => "edson@test.com"
    ],
    [
        "nome" => "João",
        "email" => "joão@test.com"
    ]
];

echo "<pre>";
print_r($usuarios);
echo "</pre>";

echo $usuarios [1]['nome'];

echo "<hr>";

foreach($listaCompra as $item) {

    echo $item . ", ";
}

echo "<hr>";

foreach($usuarios as $key => $usuario) {
    echo $key . " - ";
    echo $usuario['nome']. "<br>";
    echo $usuario['email']. "<br>";
}